
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "print.h"

#define FORMAT_FLAG_LEFT_JUSTIFY   (1u << 0)
#define FORMAT_FLAG_PAD_ZERO       (1u << 1)
#define FORMAT_FLAG_PRINT_SIGN     (1u << 2)
#define FORMAT_FLAG_ALTERNATE      (1u << 3)

#define INTEGER_NUM_MAX 9 // 整数最大位数
#define DECIMAL_NUM_MAX 8 // 小数最大位数(建议实际计算小数位数不要超过4位，否则误差较大)
#define DIGITS_NUM_MAX  (INTEGER_NUM_MAX+DECIMAL_NUM_MAX+1) // 格式化数值时最大的位数,+1:小数点占位符
typedef struct {
	unsigned FormatFlags; //! 用于格式化控制的字符
	unsigned FieldWidth; //! 格式化后数据显示的占位数
	unsigned NumDigits; //! 主要用于浮点数（该平台不支持浮点类型）
    
    unsigned fmtLen; //! 指定格式化长度
    unsigned fmtPos; //! 格式化数字到字符串buf的位置
	char fmtBuf[PRINT_BUFF_SIZE]; //! 格式化数字到字符串buf
	char fmt; //! 格式化符号类型
    unsigned short sign; //! 1 - 数值为负值
} format_ctrl_t;
format_ctrl_t fc = {
    .FormatFlags = 0,
    .FieldWidth = 0,
    .NumDigits = 0,
    
    .fmtLen = 0,
    .fmtPos = 0,
    .fmtBuf = {0},
    .fmt = '\0',
    .sign = 0,
};
char hexCharTbl[2][16] = {{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'}, 
                          {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' }};
char* get_file_name(const char* str)
{
    unsigned len = 0;

    while(*str != '\0')
    {
        len++;
        if(*str == '\\')
        {
            len = 0;
        }
        str++;
    }
    return (char*)(str-len);
}
void send_c(char* str, int* len, char c)
{
    if(str != 0) 
        str[*len++] = c;
    else 
        que_enqueue(c);
}
int print_c(const char c, format_ctrl_t* fc)
{
    fc->fmtBuf[fc->fmtPos++] = c;
    return 1;
}
int print_s(const char* s, format_ctrl_t* fc)
{
    int i = 0; //! 字符串长度
    
    if(fc->fmtLen > 0) while((unsigned)i < fc->fmtLen)
    {
        if(fc->fmtPos >= PRINT_BUFF_SIZE) 
            break; 
        fc->fmtBuf[fc->fmtPos++] = s[i++];
    } //! 如果指定了按长度格式化，则格式化指定长度的字符
    else
    {
        while((s[i] != '\0') && (i < PRINT_BUFF_SIZE)) 
        {
            if(fc->fmtPos >= PRINT_BUFF_SIZE) 
                break; 
            fc->FieldWidth = (fc->FieldWidth>0)?(fc->FieldWidth-1):0; // 位域递减
            fc->fmtBuf[fc->fmtPos++] = s[i++];
        } //! 如果没指定格式化长度，则逐个格式化，直到遇到0值则结束
    }
    return 1;
}
int print_d(int v, format_ctrl_t* fc)
{
    int i = 0;
    int j = 0;
    char chr = '\0';
	signed tmpVal = 0; //! 带符号数值临时变量
    
    tmpVal = (signed)v;
    if(tmpVal < 0)
    {
        tmpVal = ~tmpVal+1;
        fc->sign = 1;
    }
    do { // 计算整数数部分，倒叙插入buffer
        fc->FieldWidth = (fc->FieldWidth>0)?(fc->FieldWidth-1):0; // 位域递减
        fc->fmtBuf[i++] = hexCharTbl[0][tmpVal%10];
        tmpVal /= 10;
    } while(tmpVal != 0);
    fc->fmtPos = i;
    do { // 倒叙的倒叙，即顺序
        chr = fc->fmtBuf[j];
        fc->fmtBuf[j++] = fc->fmtBuf[i-1];
        fc->fmtBuf[--i] = chr;
    } while(j < i);
    return 1;
}
int print_u(int v, format_ctrl_t* fc)
{
    int i = 0;
    int j = 0;
    char chr = '\0';
	unsigned tmpVal = 0; //! 带符号数值临时变量
    
    tmpVal = (unsigned)v;
    do { // 计算整数数部分，倒叙插入buffer
        fc->FieldWidth = (fc->FieldWidth>0)?(fc->FieldWidth-1):0; // 位域递减
        fc->fmtBuf[i++] = hexCharTbl[0][tmpVal%10];
        tmpVal /= 10;
    } while(tmpVal != 0);
    fc->fmtPos = i;
    do { // 倒叙的倒叙，即顺序
        chr = fc->fmtBuf[j];
        fc->fmtBuf[j++] = fc->fmtBuf[i-1];
        fc->fmtBuf[--i] = chr;
    } while(j < i);
    return 1;
}
int print_x(int v, format_ctrl_t* fc)
{
    int i = 0;
    int j = 0;
    char chr = '\0';
	unsigned letter = 0; //! 十六进制大小写选择
	unsigned tmpVal = 0; //! 带符号数值临时变量
    
    tmpVal = (unsigned)v;
    if((fc->fmt == 'x') || (fc->fmt == 'p')) 
        letter = 1; //! 十六进制数值小写
    else if((fc->fmt == 'X') || (fc->fmt == 'P')) 
        letter = 0; //! 十六进制数值大写
    do { // 计算整数数部分，倒叙插入buffer
        fc->FieldWidth = (fc->FieldWidth>0)?(fc->FieldWidth-1):0; // 位域递减
        fc->fmtBuf[i++] = (unsigned)(hexCharTbl[letter][tmpVal&0x0000000F]);
        tmpVal >>= 4;
    } while(tmpVal != 0);
    fc->fmtPos = i;
    do { // 倒叙的倒叙，即顺序
        chr = fc->fmtBuf[j];
        fc->fmtBuf[j++] = fc->fmtBuf[i-1];
        fc->fmtBuf[--i] = chr;
    } while(j < i);
    return 1;
}
int print_f(double f, format_ctrl_t* fc)
{
    int digits = 0; // 计算小数位数个数
    int fPart = 0; // 浮点数小数部分的值（转换成整数并四舍五入）
    int dPart = (signed)f; //! 浮点数整数部分的值（带符号）
    double fVal = f-dPart; // 浮点数小数部分的值
    int i = 0;
    int j = 0;
    char chr = '\0';

    if(((f > 0) && (dPart > 1855370752)) || ((f < 0) && (dPart < -2147483647))) { // 如果超过了32位，就不处理了。。。
        dPart = 0;
        fVal = 0.0;
    }
    fc->FieldWidth = (fc->FieldWidth!=0)?fc->FieldWidth:INTEGER_NUM_MAX; // 整数位数
    fc->FieldWidth = (fc->FieldWidth>INTEGER_NUM_MAX)?INTEGER_NUM_MAX:fc->FieldWidth;
    fc->NumDigits = (fc->NumDigits!=0)?fc->NumDigits:DECIMAL_NUM_MAX; // 小数位数
    fc->NumDigits = (fc->NumDigits>DECIMAL_NUM_MAX)?DECIMAL_NUM_MAX:fc->NumDigits;
    digits = fc->NumDigits;
    while(fc->NumDigits > 0) {
        fVal *= 10; // 将要处理的小数位上升到整数位，方便处理
        fc->NumDigits--;
    }
    fPart = (signed)(fVal*10); // 小数位再临时上升1位，用作四舍五入（跟实际计算误差较大）
    if(f < 0){ // 如果负值，需要取补码
        dPart = ~dPart+1;
        fPart = ~fPart+1;
        fc->sign = 1;
    }
    fPart = (fPart+5)/10; // +5实现四舍五入，然后小数位再下降1位
    do { // 计算小数部分，倒叙插入buffer
        fc->fmtBuf[i++] = hexCharTbl[0][fPart%10];
        fPart /= 10;
        digits--;
    } while(digits > 0);
    fc->fmtBuf[i++] = '.'; // 插入小数点
    do { // 计算整数数部分，倒叙插入buffer
        fc->FieldWidth = (fc->FieldWidth>0)?(fc->FieldWidth-1):0; // 位域递减
        fc->fmtBuf[i++] = hexCharTbl[0][dPart%10];
        dPart /= 10;
    } while(dPart != 0);
    fc->fmtPos = i;
    do { // 倒叙的倒叙，即顺序
        chr = fc->fmtBuf[j];
        fc->fmtBuf[j++] = fc->fmtBuf[i-1];
        fc->fmtBuf[--i] = chr;
    } while(j < i);
    return 1;
}
int vprint(char *strBuf, unsigned size, const char *sFormat, va_list *pParamList)
{
    int rtn = 0;
    char c = '\0';
	int len = 0;
    int i = 0;
	int v; //! 用于逐个读取pParamList参数列表里面的参数值
    short bSize = (size>0)?1:0; //! 如果指定输出字符串长度，那么即使未到字符串结尾，都停止输出（调试发现，蓝牙串口接收字符串buffer不会自动清空，可能会错误输出上一次多余的内容
    
    fc.fmtLen = size;
    do {
        c = *sFormat;
		sFormat++;
        
        /**
          * 条件1：((c == 0u) && (size == 0)) - 防止数据buffer中间有0值时，被错误终止输出 
          * 条件2：((bSize == TRUE) && (size == 0)) - 当数据buffer有多余字符串时，保证指输出指定长度的字符 
          */
		if (((c == 0u) && (size == 0)) || ((bSize == 1) && (size == 0))) {
            break; //! 当遇到字符为0值时，如果长度未结束，则继续输出(当m_nprintf中未使用“%s”格式化时，会直接逐个字符输出)
        }
		if (c == '%') {
			fc.FormatFlags = 0u;
			v = 1;
			do {
				c = *sFormat;
				switch (c) {/** note: 'FormatFlags' did not achive in uart pirnt mode. add by mlw at 20190804 02:32*/
					case '-': fc.FormatFlags |= FORMAT_FLAG_LEFT_JUSTIFY; sFormat++; break;
					case '0': fc.FormatFlags |= FORMAT_FLAG_PAD_ZERO;     sFormat++; break;
					case '+': fc.FormatFlags |= FORMAT_FLAG_PRINT_SIGN;   sFormat++; break;
					case '#': fc.FormatFlags |= FORMAT_FLAG_ALTERNATE;    sFormat++; break;
					default:  v = 0; break;
				}
			} while (v);
			fc.FieldWidth = 0u;
			do {
				c = *sFormat;
				if ((c < '0') || (c > '9')) {
					break;
				}
				sFormat++;
				fc.FieldWidth = (fc.FieldWidth *10u) + ((unsigned)c - '0');
			} while (1);
			fc.NumDigits = 0u;
			c = *sFormat;
			if (c == '.') {
				sFormat++;
				do {
					c = *sFormat;
					if ((c < '0') || (c > '9')) {
						break;
					}
					sFormat++;
					fc.NumDigits = fc.NumDigits *10u + ((unsigned)c - '0');
				} while (1);
			}
			c = *sFormat;
			do {
				if ((c == 'l') || (c == 'h')) {
					sFormat++;
					c = *sFormat;
				}
				else {
					break;
				}
			} while (1);
            fc.fmt = c;
            switch(c) {
				case 'c': {
					rtn = print_c((char)va_arg(*pParamList, int), &fc);
					break;
				}
				case 's': {
					rtn = print_s(va_arg(*pParamList, const char *), &fc);
					break;
				}
				case 'd': {
					rtn = print_d(va_arg(*pParamList, int), &fc);
					break;
				}
				case 'u': {
					rtn = print_u(va_arg(*pParamList, int), &fc);
					break;
				}
				case 'p':
				case 'P':
				case 'x':
				case 'X': {
					rtn = print_x(va_arg(*pParamList, int), &fc);
					break;
				}
                case 'f': {
					rtn = print_f(va_arg(*pParamList, double), &fc);
                    break;
                }
				case '%': {
					send_c(strBuf, &len, '%');
					break;
				default:
					break;
				}
            }
            if(rtn == 1)
            {
                if((fc.sign == 1) && (fc.FieldWidth > 0)) { // 如果是负数，整数位域要-1，以便插入负号时保证对齐
                    fc.FieldWidth -= 1;
                }
				if(!(fc.FormatFlags&FORMAT_FLAG_LEFT_JUSTIFY)) //! 右对齐，则左边插入占位符
				{
                    if((fc.sign == 1) && (fc.FormatFlags&FORMAT_FLAG_PAD_ZERO)) { // 如果占位符为‘0’，则先插入负号，如果是负数的话
						send_c(strBuf, &len, '-');
                    }
					for(i = 0; i < (int)fc.FieldWidth; i++) // 插入占位符
					{
						send_c(strBuf, &len, (fc.FormatFlags&FORMAT_FLAG_PAD_ZERO)?'0':' ');
					}
                    if((fc.sign == 1) && !(fc.FormatFlags&FORMAT_FLAG_PAD_ZERO)) { // 如果占位符不为‘0’（不为‘0’则默认为空格），则先插入占位符再插入负号，如果是负数的话
						send_c(strBuf, &len, '-');
                    }
				} else if(fc.sign == 1) {
                    send_c(strBuf, &len, '-');
                }
                for(i = 0; i < fc.fmtPos; i++) // 插入内容
				{
					send_c(strBuf, &len, fc.fmtBuf[i]);
				}
				if(fc.FormatFlags&FORMAT_FLAG_LEFT_JUSTIFY) //! 左对齐，默认插入空格（无需过多复杂处理）
				{
					for(i = 0; i < (int)fc.FieldWidth; i++)
					{
						send_c(strBuf, &len, ' ');
					}
				}
                
                for(i = 0; i < (int)sizeof(fc.fmtBuf); i++) 
                    fc.fmtBuf[i] = '\0';
                fc.fmtPos =  0;
                fc.sign = 0;
            }
			sFormat++;
		}
		else
		{
            if(size > 0) 
                size--; //! 如果是带长度的格式化输出，则每输出一个字符，长度减一(当m_nprintf中未使用“%s”格式化时，会直接逐个字符输出)
			send_c(strBuf, &len, c);
		}
    } while(1);
    
    #if USE_RTT
    fc.fmtLen = rtt_get_buf(fc.fmtBuf);
    if(fc.fmtLen > 0) {
        for(i = 0; i < fc.fmtLen; i++) {
            send_c(fc.fmtBuf, &len, c);
        }
    }
    #endif
    
    return 0;
}
int print(const char *sFormat, ...)
{ // 打印输出，0结尾
    int r = 0;
    va_list ParamList;

    va_start(ParamList, sFormat);
    r = vprint(0, 0, sFormat, &ParamList);
    va_end(ParamList);
    return r;
}
int dprint(const char* file, const char* func, unsigned line, const char* level, char* color, const char *sFormat, ...)
{ // 打印输出，0结尾，比print多打印log前缀信息
    int r = 0;
    va_list ParamList;
    
    #if USE_RTT_CLOSE
    if(rtt_en == false) {
        return 0;
    }
    #endif
    if((memcmp("<notice>", level, 7) == 0) ||
       (memcmp("<uplink>", level, 7) == 0) ||
       (memcmp("<dwlink>", level, 7) == 0))
    {
        print("%-20s%-20s%6d%10s:", get_file_name(file), func, line, level);
        #if USE_RTT
        if(color != 0){
            print(color);
        }
        #endif
        print("%-11s", sFormat);
        #if USE_RTT
        SEGGER_RTT_WriteString(0, RTT_CTRL_RESET);
        #endif
        return 0;
    }
    else
    {
        #if USE_RTT
        if(color != 0){
            print(color);
        }
        #endif
        print("%-20s%-20s%6d%10s:", get_file_name(file), func, line, level);
    }
    va_start(ParamList, sFormat);
    
    r = vprint(0, 0, sFormat, &ParamList);
    va_end(ParamList);
    #if USE_RTT
    SEGGER_RTT_WriteString(0, RTT_CTRL_RESET);
    #endif
    return r;
}
int nprint(unsigned size, const char *sFormat, ...)
{ // 打印输出，指定打印长度
    int r = 0;
    va_list ParamList;

    va_start(ParamList, sFormat);
    r = vprint(0, size, sFormat, &ParamList);
    va_end(ParamList);
    return r;
}
int sprint(char *buf, const char *sFormat, ...)
{ // 将信息格式化到指定buffer，0结尾
    int r = 0;
    va_list ParamList;

    va_start(ParamList, sFormat);
    r = vprint(buf, 0, sFormat, &ParamList);
    va_end(ParamList);
    buf[r] = '\0';
    return r;
}
int snprint(char *buf, unsigned size, const char *sFormat, ...)
{ // 将信息格式化到指定buffer，指定格式化长度
    int r = 0;
    va_list ParamList;

    va_start(ParamList, sFormat);
    r = vprint(buf, size, sFormat, &ParamList);
    va_end(ParamList);
    buf[r] = '\0';
    return r;
}

