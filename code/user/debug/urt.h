
#ifndef __M_PUTS_H__

#include <stdint.h>

#define PHY_UART

#ifdef  PHY_UART
#define USE_RECV_HEX_MODE  0

uint8_t urt_read_flag(void);
uint8_t urt_read_str(uint8_t* str);
void urt_init(void);
void urt_recv(void);
unsigned urt_send(char chr);
void urt_send_block(char chr);
#endif

#endif // __M_PUTS_H__
