
#ifndef __PRINT_H__
#define __PRINT_H__

#include "SEGGER_RTT.h"
#include "rtt.h"
#include "que.h"


#define USE_RTT             0 // ucos中使用rtt模式会出错
#define USE_FIFO            0 // 如果ucos中使用了软件定时器，则使用FIFO模式会出错

#define SEND_BUFF_SIZE      255 //! 输出到串口的最大数据长度
#define RECV_BUFF_SIZE      255 //! 接收串口输入的最大数据长度
#define PRINT_BUFF_SIZE     128 //! 输出到串口前，用于格式化数据的缓存区长度

#define LOG_HELP(...)       dprint(__FILE__, __func__, __LINE__, "<help>",       RTT_CTRL_TEXT_BRIGHT_YELLOW,    __VA_ARGS__) // 文字深黄色
#define LOG_ERROR(...)      dprint(__FILE__, __func__, __LINE__, "<error>",      RTT_CTRL_BG_MAGENTA,            __VA_ARGS__) // 背景浅洋红色
#define LOG_WARNING(...)    dprint(__FILE__, __func__, __LINE__, "<warning>",    RTT_CTRL_BG_YELLOW,             __VA_ARGS__) // 背景浅黄色
#define LOG_INFO(...)       dprint(__FILE__, __func__, __LINE__, "<info>",       RTT_CTRL_TEXT_BRIGHT_RED,       __VA_ARGS__) // 文字深红色
#define LOG_DEBUG(...)      dprint(__FILE__, __func__, __LINE__, "<debug>",      RTT_CTRL_TEXT_GREEN,            __VA_ARGS__) // 文字浅绿色
#define LOG_TIPS(...)       dprint(__FILE__, __func__, __LINE__, "<tips>",       RTT_CTRL_BG_GREEN,              __VA_ARGS__) // 背景浅绿色
#define LOG_NORMAL(...)     dprint(__FILE__, __func__, __LINE__, "<normal>",     0,                              __VA_ARGS__) // 黑底白字（默认）
#define LOG_NOTICE(...)     dprint(__FILE__, __func__, __LINE__, "<notice>",     RTT_CTRL_BG_BRIGHT_BLUE,        __VA_ARGS__) // 关键词背景深蓝色，其它黑底白字
#define LOG_UPLINK(...)     dprint(__FILE__, __func__, __LINE__, "<uplink>",     RTT_CTRL_BG_RED,                __VA_ARGS__)
#define LOG_DOWNLINK(...)   dprint(__FILE__, __func__, __LINE__, "<dwlink>",     RTT_CTRL_BG_GREEN,              __VA_ARGS__)
#define LOG_PRINT(...)      print(__VA_ARGS__) // 传统打印模式

int print(const char *sFormat, ...);
int dprint(const char* file, const char* func, unsigned line, const char* level, char* color, const char *sFormat, ...);
int nprint(unsigned size, const char *sFormat, ...);
int sprint(char *buf, const char *sFormat, ...);
int snprint(char *buf, unsigned size, const char *sFormat, ...);
    
#endif
