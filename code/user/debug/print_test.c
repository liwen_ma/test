
#if 0
#include "print.h"
void m_printf_test(void) {   
  unsigned char cnt = 0;
  static volatile int _Cnt;
  signed char val1 = -125;
//  unsigned char val2 = 125;
  unsigned char buf[] = {"maliwen"};
  unsigned short hexV = 0x56e;
  float vPi = -123.141592465;
 
  #if USE_RTT
  LOG_DEBUG(RTT_CTRL_TEXT_RED"%02d test: pi = %f, val1 = %d, buf(%p) = %s, hexV = 0x%05X\r\n", cnt++, vPi, val1, buf, buf,hexV);
  LOG_DEBUG(RTT_CTRL_TEXT_RED"%02d test: pi = %3.12f, val1 = %d, buf(%P) = %s, hexV = 0x%05X\r\n", cnt++, vPi, val1, buf, buf,hexV);
  LOG_DEBUG(RTT_CTRL_TEXT_RED"%02d test: pi = %3.12f, val1 = %d, buf(%p) = %s, hexV = 0x%05X\r\n", cnt++, vPi, val1, buf, buf,hexV);
  LOG_DEBUG(RTT_CTRL_TEXT_RED"%02d test: pi = %3.12f, pi = %3.12f, pi = %3.12f, val1 = %d, buf(%P) = %s, hexV = 0x%05X\r\n", cnt++, vPi, vPi, vPi, val1, buf, buf,hexV);
  #else
  LOG_DEBUG("%02d test: pi = %f, val1 = %d, buf(%p) = %s, hexV = 0x%05X\r\n", cnt++, vPi, val1, buf, buf,hexV);
  LOG_DEBUG("%02d test: pi = %3.12f, val1 = %d, buf(%P) = %s, hexV = 0x%05X\r\n", cnt++, vPi, val1, buf, buf,hexV);
  LOG_DEBUG("%02d test: pi = %3.12f, val1 = %d, buf(%p) = %s, hexV = 0x%05X\r\n", cnt++, vPi, val1, buf, buf,hexV);
  LOG_DEBUG("%02d test: pi = %3.12f, pi = %3.12f, pi = %3.12f, val1 = %d, buf(%P) = %s, hexV = 0x%05X\r\n", cnt++, vPi, vPi, vPi, val1, buf, buf,hexV);
  #endif
  LOG_DEBUG("%s\r\n", "maliwen test at 20190804 1725");
  LOG_DEBUG("%s\r\n", "maliwen");
  LOG_DEBUG("%-10s:%s\r\n", "name", "maliwen");
  LOG_DEBUG("%+10s:%s\r\n", "name", "maliwen");
  
  LOG_DEBUG("%02d SEGGER Real-Time-Terminal Sample\r\n\r\n", cnt++);
  LOG_DEBUG("%02d ###### Testing SEGGER_printf() ######\r\n", cnt++);

  LOG_DEBUG("%+02d printf Test: %%4.5f,     123.45 : %4.5f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%04.5f,    123.45 : %04.5f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%-4.5f,    123.45 : %-4.5f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%-04.5f,   123.45 : %-04.5f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%+8.5f,    123.45 : %+8.5f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%+08.5f,   123.45 : %+08.5f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%8.3f,     123.45 : %8.3f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%08.3f,    123.45 : %08.3f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%-8.3f,    123.45 : %-8.3f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%-08.3f,   123.45 : %-08.3f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%+8.3f,    123.45 : %+8.3f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%+08.3f,   123.45 : %+08.3f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%+02d printf Test: %%8.3f,     -123.45 : %8.3f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%+02d printf Test: %%08.3f,    -123.45 : %08.3f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%+02d printf Test: %%-8.3f,    -123.45 : %-8.3f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%+02d printf Test: %%-08.3f,   -123.45 : %-08.3f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%+02d printf Test: %%+8.3f,    -123.45 : %+8.3f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%+02d printf Test: %%+08.3f,   -123.45 : %+08.3f.\r\n", cnt++, -123.45);

  #if USE_RTT
  LOG_DEBUG(RTT_CTRL_TEXT_YELLOW"%02d printf Test: %%c,         'S' : %c.\r\n", cnt++, 'S');
  #else
  LOG_DEBUG("%02d printf Test: %%c,         'S' : %c.\r\n", cnt++, 'S');
  #endif
  LOG_DEBUG("%02d printf Test: %%5c,        'E' : %5c.\r\n", cnt++, 'E');
  LOG_DEBUG("%02d printf Test: %%-5c,       'G' : %-5c.\r\n", cnt++, 'G');
  LOG_DEBUG("%02d printf Test: %%5.3c,      'G' : %-5c.\r\n", cnt++, 'G');
  LOG_DEBUG("%02d printf Test: %%.3c,       'E' : %-5c.\r\n", cnt++, 'E');
  LOG_DEBUG("%02d printf Test: %%c,         'R' : %c.\r\n", cnt++, 'R');

  LOG_DEBUG("%02d printf Test: %%s,      \"RTT\" : %s.\r\n", cnt++, "RTT");
  #if USE_RTT
  LOG_DEBUG(RTT_CTRL_TEXT_BRIGHT_YELLOW"%02d printf Test: %%s, \"RTT\\r\\nRocks.\" : %s.\r\n", cnt++, "RTT\r\nRocks.");
  #else
  LOG_DEBUG("%02d printf Test: %%s, \"RTT\\r\\nRocks.\" : %s.\r\n", cnt++, "RTT\r\nRocks.");
  #endif

  LOG_DEBUG("%+02d %%d %d, %8.3d, %2.4d, %d, %10.6d.\r\n", cnt++, 12345, 12345, 12345, 12345, 12345);
  LOG_DEBUG("%+02d %%d %d, %8.3d, %2.4d, %d, %10.6d.\r\n", cnt++, -12345, -12345, -12345, -12345, -12345);
  LOG_DEBUG("%+02d %%u %u, %8.3u, %2.4u, %u, %10.6u.\r\n", cnt++, 12345, 12345, 12345, 12345, 12345);
  LOG_DEBUG("%+02d %%u %u, %8.3u, %2.4u, %u, %10.6u.\r\n", cnt++, -12345, -12345, -12345, -12345, -12345);
  
  LOG_DEBUG("%+02d printf Test: %%8.3d,    12345 : %8.3d.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%08.3d,   12345 : %08.3d.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%-8.3d,   12345 : %-8.3d.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%-08.3d,  12345 : %-08.3d.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%+8.3d,   12345 : %+8.3d.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%+08.3d,  12345 : %+08.3d.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%8.3d,    -12345 : %8.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%+02d printf Test: %%08.3d,   -12345 : %08.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%+02d printf Test: %%-8.3d,   -12345 : %-8.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%+02d printf Test: %%-08.3d,  -12345 : %-08.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%+02d printf Test: %%+8.3d,   -12345 : %+8.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%+02d printf Test: %%+08.3d,  -12345 : %+08.3d.\r\n", cnt++, -12345);
  
  LOG_DEBUG("%+02d printf Test: %%8.3u,    12345 : %8.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%08.3u,   12345 : %08.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%-8.3u,   12345 : %-8.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%-08.3u,  12345 : %-08.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%+8.3u,   12345 : %+8.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%+02d printf Test: %%+08.3u,  12345 : %+08.3u.\r\n", cnt++, 12345);
  
  LOG_DEBUG("%+02d printf Test: %%8.3x,     0x123aB : %8.3x.\r\n", cnt++, 0x123aB);
  LOG_DEBUG("%+02d printf Test: %%08.3X,    0x123aB : %08.3X.\r\n", cnt++, 0x123aB);
  LOG_DEBUG("%+02d printf Test: %%-8.3x,    0x123aB : %-8.3x.\r\n", cnt++, 0x123aB);
  LOG_DEBUG("%+02d printf Test: %%-08.3X,   0x123aB : %-08.3X.\r\n", cnt++, 0x123aB);
  LOG_DEBUG("%+02d printf Test: %%+8.3x,    0x123aB : %+8.3x.\r\n", cnt++, 0x123aB);
  LOG_DEBUG("%+02d printf Test: %%+08.3X,   0x123aB : %+08.3X.\r\n", cnt++, 0x123aB);
  
  LOG_DEBUG("%02d printf Test: %%f,      -1234.5 : %f.\r\n", cnt++, -1234.5);
  LOG_DEBUG("%02d printf Test: %%+f,     -123.45 : %+f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%02d printf Test: %%.3f,    -12.345 : %.3f.\r\n", cnt++, -12.345);
  LOG_DEBUG("%02d printf Test: %%.6f,    -1.2345 : %.6f.\r\n", cnt++, -1.2345);
  LOG_DEBUG("%02d printf Test: %%6.3f,   -1234.5 : %6.3f.\r\n", cnt++, -1234.5);
  LOG_DEBUG("%02d printf Test: %%8.6f,   -123.45 : %8.6f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%02d printf Test: %%08f,    -12.345 : %08f.\r\n", cnt++, -12.345);
  LOG_DEBUG("%02d printf Test: %%08.6f,  -1.2345 : %08.6f.\r\n", cnt++, -1.2345);
  LOG_DEBUG("%02d printf Test: %%0f,     -1234.5 : %0f.\r\n", cnt++, -1234.5);
  LOG_DEBUG("%02d printf Test: %%-.6f,   -123.45 : %-.6f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%02d printf Test: %%-6.3f,  -12.345 : %-6.3f.\r\n", cnt++, -12.345);
  LOG_DEBUG("%02d printf Test: %%-8.6f,  -1.2345 : %-8.6f.\r\n", cnt++, -1.2345);
  LOG_DEBUG("%02d printf Test: %%-08f,   -1234.5 : %-08f.\r\n", cnt++, -1234.5);
  LOG_DEBUG("%02d printf Test: %%-08.6f, -123.45 : %-08.6f.\r\n", cnt++, -123.45);
  LOG_DEBUG("%02d printf Test: %%-0f,    -12.345 : %-0f.\r\n", cnt++, -12.345);

  LOG_DEBUG("%02d printf Test: %%f,      1234.5 : %f.\r\n", cnt++, 1234.5);
  LOG_DEBUG("%02d printf Test: %%+f,     123.45 : %+f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%02d printf Test: %%.3f,    12.345 : %.3f.\r\n", cnt++, 12.345);
  LOG_DEBUG("%02d printf Test: %%.6f,    1.2345 : %.6f.\r\n", cnt++, 1.2345);
  LOG_DEBUG("%02d printf Test: %%6.3f,   1234.5 : %6.3f.\r\n", cnt++, 1234.5);
  LOG_DEBUG("%02d printf Test: %%8.6f,   123.45 : %8.6f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%02d printf Test: %%08f,    12.345 : %08f.\r\n", cnt++, 12.345);
  LOG_DEBUG("%02d printf Test: %%08.6f,  1.2345 : %08.6f.\r\n", cnt++, 1.2345);
  LOG_DEBUG("%02d printf Test: %%0f,     1234.5 : %0f.\r\n", cnt++, 1234.5);
  LOG_DEBUG("%02d printf Test: %%-.6f,   123.45 : %-.6f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%02d printf Test: %%-6.3f,  12.345 : %-6.3f.\r\n", cnt++, 12.345);
  LOG_DEBUG("%02d printf Test: %%-8.6f,  1.2345 : %-8.6f.\r\n", cnt++, 1.2345);
  LOG_DEBUG("%02d printf Test: %%-08f,   1234.5 : %-08f.\r\n", cnt++, 1234.5);
  LOG_DEBUG("%02d printf Test: %%-08.6f, 123.45 : %-08.6f.\r\n", cnt++, 123.45);
  LOG_DEBUG("%02d printf Test: %%-0f,    12.345 : %-0f.\r\n", cnt++, 12.345);

  LOG_DEBUG("%02d printf Test: %%u,       12345 : %u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%+u,      12345 : %+u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%.3u,     12345 : %.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%.6u,     12345 : %.6u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%6.3u,    12345 : %6.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%8.6u,    12345 : %8.6u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%08u,     12345 : %08u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%08.6u,   12345 : %08.6u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%0u,      12345 : %0u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%-.6u,    12345 : %-.6u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%-6.3u,   12345 : %-6.3u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%-8.6u,   12345 : %-8.6u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%-08u,    12345 : %-08u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%-08.6u,  12345 : %-08.6u.\r\n", cnt++, 12345);
  LOG_DEBUG("%02d printf Test: %%-0u,     12345 : %-0u.\r\n", cnt++, 12345);

  LOG_DEBUG("%02d printf Test: %%u,      -12345 : %u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%+u,     -12345 : %+u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%.3u,    -12345 : %.3u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%.6u,    -12345 : %.6u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%6.3u,   -12345 : %6.3u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%8.6u,   -12345 : %8.6u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%08u,    -12345 : %08u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%08.6u,  -12345 : %08.6u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%0u,     -12345 : %0u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-.6u,   -12345 : %-.6u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-6.3u,  -12345 : %-6.3u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-8.6u,  -12345 : %-8.6u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-08u,   -12345 : %-08u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-08.6u, -12345 : %-08.6u.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-0u,    -12345 : %-0u.\r\n", cnt++, -12345);

  LOG_DEBUG("%02d printf Test: %%d,      -12345 : %d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%+d,     -12345 : %+d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%.3d,    -12345 : %.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%.6d,    -12345 : %.6d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%6.3d,   -12345 : %6.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%8.6d,   -12345 : %8.6d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%08d,    -12345 : %08d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%08.6d,  -12345 : %08.6d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%0d,     -12345 : %0d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-.6d,   -12345 : %-.6d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-6.3d,  -12345 : %-6.3d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-8.6d,  -12345 : %-8.6d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-08d,   -12345 : %-08d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-08.6d, -12345 : %-08.6d.\r\n", cnt++, -12345);
  LOG_DEBUG("%02d printf Test: %%-0d,    -12345 : %-0d.\r\n", cnt++, -12345);

  LOG_DEBUG("%02d printf Test: %%x,      0x1234ABC : %x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%+x,     0x1234ABC : %+x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%.3x,    0x1234ABC : %.3x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%.6x,    0x1234ABC : %.6x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%6.3x,   0x1234ABC : %6.3x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%8.6x,   0x1234ABC : %8.6x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%08x,    0x1234ABC : %08x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%08.6x,  0x1234ABC : %08.6x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%0x,     0x1234ABC : %0x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%-.6x,   0x1234ABC : %-.6x.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%-6.3x,  0x1234ABC : %-6.3X.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%-8.6x,  0x1234ABC : %-8.6X.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%-08x,   0x1234ABC : %-08X.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%-08.6x, 0x1234ABC : %-08.6X.\r\n", cnt++, 0x1234ABC);
  LOG_DEBUG("%02d printf Test: %%-0x,    0x1234ABC : %-0x.\r\n", cnt++, 0x1234ABC);

  LOG_DEBUG("%02d printf Test: %%p,      &_Cnt      : %p.\r\n", cnt++, &_Cnt);

  #if USE_RTT
  LOG_DEBUG(RTT_CTRL_TEXT_GREEN"%02d ###### SEGGER_printf() Tests done. ######\r\n", cnt++);
  #else
  LOG_DEBUG("%02d ###### SEGGER_printf() Tests done. ######\r\n", cnt++);
  #endif
}
#endif
