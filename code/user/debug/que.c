
#include <stdint.h>
#include "urt.h"
#include "print.h"

#if USE_FIFO

#ifdef USE_OS
#include "os.h"
#endif

#define QUE_DEEP    256
typedef struct {
	volatile uint8_t buffer[QUE_DEEP];
    volatile uint16_t head;
    volatile uint16_t tail;
}fifo_t;
static fifo_t fifo = {
	.buffer[0] = 0,
	.head = 0,
	.tail = 0,
};

void que_enqueue(char byte)
{
//    #ifdef USE_OS
//	CPU_SR_ALLOC();
//	OS_CRITICAL_ENTER();
//    #endif
    
    /** queue is full, discard the oldest byte */
    if(fifo.head == ((fifo.tail+1)%QUE_DEEP)) {
        fifo.head = (fifo.head+1)%QUE_DEEP;
    }
    fifo.buffer[fifo.tail] = byte;
    fifo.tail = (fifo.tail+1)%QUE_DEEP;

//    #ifdef USE_OS    
//	OS_CRITICAL_EXIT();	
//    #endif
}
void que_dequeue(void)
{
    /** queue is empty */
    if(fifo.head == fifo.tail) {
        return;
    }
    #if USE_RTT
    if(rtt_send_byte(fifo.buffer[fifo.head]) == 1) {
        fifo.head = (fifo.head+1)%QUE_DEEP;
    }
    #else
    if(urt_send(fifo.buffer[fifo.head]) == 0) {
        fifo.head = (fifo.head+1)%QUE_DEEP;
    }
    #endif
}
#else
void que_enqueue(char byte)
{
    #if USE_RTT
    rtt_send_byte(byte);
    #else
    urt_send_block(byte);
    #endif
}
#endif
