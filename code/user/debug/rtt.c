
#include <stdint.h>
#include "SEGGER_RTT.h"
#include "print.h"

#if USE_RTT

#define BLOCK_TIME    0xFFF   //! 当没连接jlink时，需要一个判断超时的时间，设置rtt进入非阻塞模式

typedef struct
{
    char      pBuffer[PRINT_BUFF_SIZE];
    unsigned  BufferSize;
    unsigned  Cnt;
    int   ReturnValue;
    unsigned RTTBufferIndex;
} SEGGER_RTT_PRINTF_DESC;
SEGGER_RTT_PRINTF_DESC rtt;

unsigned block_check(unsigned numByte, unsigned wrByte)
{
    static unsigned int cntDown1 = BLOCK_TIME;
    static unsigned int cntDown2 = BLOCK_TIME;
    
    if(_SEGGER_RTT.aUp[0].Flags == SEGGER_RTT_MODE_NO_BLOCK_TRIM)
    {
        return 0;
    }
    
    /** wrByte非0，表示rtt写入成功，不需要做下一步判断 */
    if(wrByte != 0)
    {
        cntDown1 = BLOCK_TIME;
        cntDown2 = BLOCK_TIME;
        return numByte;
    }
    else if(cntDown1 != 0)
    {
        cntDown1--;
    }
    else if(cntDown2 != 0)
    {
        cntDown2--;
    }
    
    /** 如果wrByte为0，说明rtt阻塞，则倒计一定数值，如果还是阻塞，说明可能rtt没有连接，设置rtt为非阻塞模式 */
    if((cntDown1 == 0) && (cntDown2 == 0))
    {
        #if USE_RTT_CLOSE
        rtt_en = false;
        #else
        cntDown1 = BLOCK_TIME;
        cntDown2 = BLOCK_TIME;
        _SEGGER_RTT.aUp[0].Flags = SEGGER_RTT_MODE_NO_BLOCK_TRIM;
        #endif
        return 0;
    }
    return numByte;
}
unsigned rtt_recv(char* msgBuf)
{
    unsigned short msgLen = 0;
    #if USE_FULL_PRINTF && !USE_SMALLEST_PRINTF
    unsigned char res = 0;
    #endif
    
    msgLen = SEGGER_RTT_Read(0, (void*)msgBuf, RECV_BUFF_SIZE);
    
    if(msgLen == 0)
    {
        return 0;
    }
    msgBuf[msgLen] = '\0';
    
    /** 帮助 */
    if((msgLen == 1) && (msgBuf[0] == '?'))
    {
        LOG_HELP("*******************************\r\n");
        LOG_HELP("* 1. !! reset the system      *\r\n");
        LOG_HELP("* 2. = clear the rtt window   *\r\n");
        LOG_HELP("* 3. - set rtt to block mode  *\r\n");
        LOG_HELP("*******************************\r\n");
        return 0;
    }
    /** 清屏 */
    if((msgLen == 1) && (msgBuf[0] == '='))
    {
        SEGGER_RTT_WriteString(0, RTT_CTRL_CLEAR);
        return 0;
    }
    /** 阻塞模式 */
    if((msgLen == 1) && (msgBuf[0] == '-'))
    {
        #if USE_RTT_CLOSE
        rtt_en = true;
        #else
        SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);
        #endif
        return 0;
    }
    /** 重启 */
    if((msgLen == 2) && (msgBuf[0] == '!') && (msgBuf[1] == '!'))
    {
        //reboot();
        ///NVIC_SystemReset();
        return 0;
    }
    
    /** 如果接收到有rtt发送过来的数据，则转换为十六进制的数值串数 */
//    LOG_INFO("%s\r\n", msgBuf);
//    msgLen = m_chrstr_to_hexstr(msgBuf, msgLen);

    return msgLen;
}
void rtt_handle(void)
{
    uint8_t data[255];
    uint16_t len;
    
    len = rtt_recv((char*)data);
    if(len > 0)
    {
        LOG_INFO("%s\r\n", data);
    }
}
int rtt_get_buf(char* buf)
{
    if (rtt.ReturnValue > 0) 
    {
        //
        // Write remaining data, if any
        //
        if (rtt.Cnt != 0u)
        {
            for(unsigned i = 0; i < rtt.Cnt; i++) {
                buf[i] = rtt.pBuffer[i];
            }
        }
        rtt.ReturnValue += (int)rtt.Cnt;
    }
    return rtt.ReturnValue;
}
unsigned rtt_send_byte(char byte)
{
    return SEGGER_RTT_Write(rtt.RTTBufferIndex, (const void*)&byte, 1);
}
void rtt_init(void)
{
    SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);
}
#endif
