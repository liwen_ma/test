
#ifndef __RTT_H__
#define __RTT_H__

unsigned block_check(unsigned numByte, unsigned wrByte);
void rtt_handle(void);
int rtt_get_buf(char* buf);
unsigned rtt_send_byte(char byte);
void rtt_init(void);

#endif // __RTT_H__
