
#ifndef __QUE_H__
#define __QUE_H__

void que_enqueue(char byte);
void que_dequeue(void);

#endif // __QUE_H__
