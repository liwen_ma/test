
#include <string.h>
#include "urt.h"
#include "print.h"
#include "gd32f30x.h"
#include "gd32f30x_rcu.h"

#if  !USE_RTT
typedef void    (*urt_uart_clock_t)     (rcu_periph_enum periph);
typedef enum {
    UART_RCVD_STEP_HEAD1,                   // 数据接收步骤：接收同步字第一个字节
    UART_RCVD_STEP_HEAD2,                   // 数据接收步骤：接收同步字第二个字节
    UART_RCVD_STEP_LENGTH,                  // 数据接收步骤：数据帧长度字节
    UART_RCVD_STEP_PAYLOAD,                 // 数据接收步骤：payload部分
    
    UART_RCVD_STEP_HEAD1_VALUE   = 0x55,    // 同步字第一个字节值
    UART_RCVD_STEP_HEAD2_VALUE   = 0xAA,    // 同步字第二个字节值
} urt_rcvd_step_t;
typedef struct {
    uint8_t *rcvd_ptr;                      // 数据接收存放buffer
    uint8_t rcvd_len;                       // 数据帧接收长度（没接收一个单元，相应自增）
    uint8_t rcvd_done;                      // 数据帧是否接收完成
    uint8_t sync_rcvd;                      // 是否接收到数据帧的同步头（此工程为：0x55AA)
    uint8_t total_len;                      // 串口接收剩余总长度
    urt_rcvd_step_t step;                 // UART数据接收当前步骤
    uint32_t recv_time;                     // UART接收超时计算
    uint8_t rcvd_ready;                     // UART数据接收就绪；读清零
}urt_recv_state_t;
typedef struct {
    urt_uart_clock_t uart_pb;             // 串口时钟设置函数
    urt_uart_clock_t gpio_pb;             // GPIO端口时钟设置函数
    uint32_t uart_clock;                    // 串口时钟
    uint32_t gpio_clock;                    // GPIO端口时钟
    uint32_t uart_port;               // UART端口
    uint32_t gpio_port;                // GPIO端口
    uint32_t tx_pin;                        // 串口发送引脚
    uint32_t rx_pin;                        // 串口接收引脚
    uint16_t tx_pin_af;                     // IO口第二功能-串口发送
    uint16_t rx_pin_af;                     // IO口第二功能-串口接收
    uint8_t port_af;                        // 备用功能选择
    uint32_t baudrate;                      // 串口波特率
} urt_uart_cfg_t;
typedef struct {
    uint8_t irq_chnl;                       // 串口收发中断通道
} urt_nvic_cfg_t;
typedef struct {
    urt_uart_cfg_t uart_cfg;              // UART配置
    urt_nvic_cfg_t nvic_cfg;              // UART中断配置
    urt_recv_state_t recv_state;          // UART接收状态机
} urt_t;

#define DEBUG_BUF_LEN 128
uint8_t urt_buffer[DEBUG_BUF_LEN];
urt_t urt = {
    .uart_cfg = {
        .uart_pb = rcu_periph_clock_enable,
        .gpio_pb = rcu_periph_clock_enable,
        .uart_clock = RCU_UART3,
        .gpio_clock = RCU_GPIOC,
        .uart_port = UART3,
        .gpio_port = GPIOC,
        .tx_pin = GPIO_PIN_10,
        .rx_pin = GPIO_PIN_11,
//        .tx_pin_af = GPIO_PinSource2,
//        .rx_pin_af = GPIO_PinSource3,
//        .port_af = GPIO_AF_1,
        .baudrate = 115200,
    },
    .nvic_cfg = {
        .irq_chnl = USART2_IRQn,
    },
    .recv_state = {
        .rcvd_ptr = urt_buffer,
        .rcvd_len = 0,
        .sync_rcvd = 0,
        .step = UART_RCVD_STEP_HEAD1,
        .total_len = 0,
        .recv_time = 0,
        .rcvd_ready = 0,
    },
};
uint8_t urt_read_flag(void)
{
    uint8_t rcvd_ready = urt.recv_state.rcvd_ready;
    
    urt.recv_state.rcvd_ready = 0;
    return rcvd_ready;
}
uint8_t urt_read_str(uint8_t* str)
{
    uint8_t len = urt.recv_state.total_len;
    if(len > 0) {
        memcpy(str, urt.recv_state.rcvd_ptr, urt.recv_state.total_len);
        memset(urt.recv_state.rcvd_ptr, 0, urt.recv_state.total_len);
        urt.recv_state.total_len = 0;
    }
    return len;
}
void urt_rcvd_handler(char chr)
{
    #if USE_RECV_HEX_MODE
    switch((uint8_t)urt.recv_state.step) {
        case UART_RCVD_STEP_HEAD1:
            if(chr != UART_RCVD_STEP_HEAD1_VALUE) {
               break;
            }
            
            urt.recv_state.sync_rcvd = 1;
            urt.recv_state.step = UART_RCVD_STEP_HEAD2;
            break;
        case UART_RCVD_STEP_HEAD2:
            if((chr != (char)UART_RCVD_STEP_HEAD2_VALUE) || (urt.recv_state.sync_rcvd != 1)) {
                urt.recv_state.sync_rcvd = 0;
                urt.recv_state.step = UART_RCVD_STEP_HEAD1;
                break;
            }
            memset(urt.recv_state.rcvd_ptr, 0, DEBUG_BUF_LEN);
            urt.recv_state.rcvd_len = 0;
            urt.recv_state.rcvd_ptr[urt.recv_state.rcvd_len++] = UART_RCVD_STEP_HEAD1_VALUE;
            urt.recv_state.rcvd_ptr[urt.recv_state.rcvd_len++] = UART_RCVD_STEP_HEAD2_VALUE;
            
            urt.recv_state.sync_rcvd = 0;
            urt.recv_state.step = UART_RCVD_STEP_LENGTH;
            urt.recv_state.recv_time = 0;
            break;
        case UART_RCVD_STEP_LENGTH:
            urt.recv_state.rcvd_ptr[urt.recv_state.rcvd_len++] = chr;
            urt.recv_state.total_len = chr;
        
            urt.recv_state.sync_rcvd = 0;
            urt.recv_state.step = UART_RCVD_STEP_PAYLOAD;
            break;
        case UART_RCVD_STEP_PAYLOAD:
            urt.recv_state.rcvd_ptr[urt.recv_state.rcvd_len++] = chr;
            if(urt.recv_state.rcvd_len >= (urt.recv_state.total_len+3)) { // +3 = 2sync+1length
                urt.recv_state.rcvd_done = 1;
                urt.recv_state.total_len = 0; // 为下次接收做准备
                urt.recv_state.sync_rcvd = 0;
                urt.recv_state.step = UART_RCVD_STEP_HEAD1;
            }
            break;
        default:
            break;
    }
    #else
    if(urt.recv_state.rcvd_len == 0) {
        memset(urt.recv_state.rcvd_ptr, 0, DEBUG_BUF_LEN);
    }
    urt.recv_state.step = UART_RCVD_STEP_PAYLOAD;
    urt.recv_state.rcvd_ptr[urt.recv_state.rcvd_len++] = chr;
    #endif
}
void urt_recv(void)
{
    if(urt.recv_state.step >= UART_RCVD_STEP_LENGTH) {
        urt.recv_state.recv_time++;
    }
    
    #if USE_RECV_HEX_MODE
    // 当接收到结束标志位，或者接收超时（1000个空闲任务周期），会认为串口接收完毕
    if((urt.recv_state.rcvd_done == 1) || (urt.recv_state.recv_time >= 1000)) {
        for(unsigned i = 0; i < urt.recv_state.rcvd_len; i++) {
            LOG_PRINT("%02X ", urt.recv_state.rcvd_ptr[i]);
        }
        LOG_PRINT("(%d)\r\n", urt.recv_state.recv_time);
        urt.recv_state.rcvd_done = 0;
        urt.recv_state.rcvd_len = 0;
        urt.recv_state.recv_time = 0;
        urt.recv_state.step = UART_RCVD_STEP_HEAD1;
        urt.recv_state.rcvd_ready = 1;
    }
    #else
    if(urt.recv_state.recv_time >= 1000) {
        urt.recv_state.rcvd_ptr[urt.recv_state.rcvd_len] = '\0';
        LOG_PRINT("%s", urt.recv_state.rcvd_ptr);
        urt.recv_state.total_len = urt.recv_state.rcvd_len;
        urt.recv_state.recv_time = 0;
        urt.recv_state.rcvd_len = 0;
        urt.recv_state.step = UART_RCVD_STEP_HEAD1;
        urt.recv_state.rcvd_ready = 1;
    }
    #endif
}
unsigned urt_send(char chr)
{
    if(usart_flag_get(UART3, USART_FLAG_TBE) == RESET) {
        return 1;
    }
    usart_data_transmit(UART3, (uint8_t) chr);
    return 0;
}
void urt_send_block(char chr)
{
    while(usart_flag_get(UART3, USART_FLAG_TBE) == RESET);
    usart_data_transmit(UART3, (uint8_t) chr);
}
// 中断配置
void urt_nvic_config(urt_t *cfg)
{
}
// 串口配置
void urt_uart_config(urt_t *cfg)
{
    /* enable GPIO clock */
    rcu_periph_clock_enable(RCU_GPIOC);

    /* enable USART clock */
    rcu_periph_clock_enable(RCU_UART3);

    /* connect port to USARTx_Tx */
    gpio_init(GPIOC, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_10);

    /* connect port to USARTx_Rx */
    gpio_init(GPIOC, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_11);

    /* USART configure */
    usart_deinit(UART3);
    usart_baudrate_set(UART3, 115200U);
    usart_receive_config(UART3, USART_RECEIVE_ENABLE);
    usart_transmit_config(UART3, USART_TRANSMIT_ENABLE);
    usart_enable(UART3);
}
void urt_init(void)
{
    urt_uart_config(&urt);
//    urt_nvic_config(&urt);
}
#endif
